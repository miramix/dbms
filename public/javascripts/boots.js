angular.module("dbms", [
    "ngResource",
    "ui.router"
]);

angular.module("dbms").config(["$stateProvider", "$urlRouterProvider", "$locationProvider", function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state("home", {
        url: "/",
        templateUrl: "views/home/index.html",
        controller: "HomeController:index" //"HomeIndexController"
    });

    $stateProvider.state("tables", {
        url: "/tables",
        templateUrl: "views/tables/index.html",
        controller: "TablesController"
    });

    $stateProvider.state("books", {
        url: "/books",
        templateUrl: "views/books/index.html",
        controller: "BooksController"
    });

    $stateProvider.state("users", {
        url: "/users",
        abstract: true
    });

    $stateProvider.state("users.index", {
        url: "",
        views: {
            "@": {
                templateUrl: "views/users/index.html",
                controller: "UserController:index"
            }
        }
    });

    $stateProvider.state("users.item", {
        url: "/{id}",
        params: {
            code: {
                squash: false,
                value: null
            }
        },
        views: {
            "@": {
                templateUrl: "views/users/item.html",
                controller: "UserController:item"
            }
        }
    });

    $stateProvider.state("error", {
        url: "/error/{code}",
        params: {
            code: {
                squash: false,
                value: "404"
            }
        },
        templateUrl: "views/error.html",
        controller: ["$scope", "$state", function ($scope, $state) {
            $scope.code = $state.params.code;
        }]
    });

    $urlRouterProvider.otherwise("/error/404");

    $locationProvider.html5Mode(true);
}]);

angular.module("dbms").run(["$rootScope", "$state", function ($rootScope, $state) {
    $rootScope.$on("$stateChangeError", function() {
        $state.go("error", { code: "500" }, { location: false });
    });

    $rootScope.$on("$stateNotFound", function() {
        $state.go("error", { code: "404" }, { location: false });
    });
}]);
