angular.module("dbms").factory("config", ["$location", function ($location) {
    var path = $location.absUrl().replace("" + $location.url(), ''); // #

    if(path.substr(-1) === "/") {
        path = path.substr(0, path.length - 1);
    }

    return {
        api: path + "/api",
        path: path
    }
}]);
