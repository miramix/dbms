angular.module("dbms").factory("Users", ["$resource", "config", function($resource, config) {
    return $resource(config.api + "/users/:id");
}]);
