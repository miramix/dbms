angular.module("dbms").controller("UserController:index", ["$scope", "$state", "Users", function ($scope, $state, Users) {
    Users.query(function (users) {
        if(users.length) {
            $scope.users = users;
        } else {
            $state.go("error", { code: "404" }, { location: false });
        }
    });
}]);

angular.module("dbms").controller("UserController:item", ["$scope", "$state", "Users", function ($scope, $state, Users) {
    Users.get({ id: $state.params.id || 0 }, function (user) {
        if(user._id) {
            $scope.user = user;
        } else {
            $state.go("error", { code: "404" }, { location: false });
        }
    });
}]);
