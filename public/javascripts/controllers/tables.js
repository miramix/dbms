angular.module("dbms").controller("TablesController", ["$scope", "$state", "Users", function ($scope, $state, Users) {
    $scope.state = "append";

    Users.query({}, function (users) {
        if(users.length) {
            $scope.users = users;
        } else {
            $state.go("error", { code: "404" }, { location: false });
        }
    });

    $scope.form = {
        id: '',
        name: '',
        surname: '',
        phone: ''
    };

    $scope.save = function(id) {
        console.log('save - ' + id);

        Users.save({ xz: 1 }, {
            qwe: 1,
            cxvbcvb: 1,
            cvbcv: 43
        }, function (response) {
            console.log(response);
        });
    };

    $scope.edit = function(id) {
        console.log('edit - ' + id);
    };

    $scope.delete = function(id) {
        console.log('delete - ' + id);
    };

    $scope.select = function(user) {
        $scope.form.id = user.id;
        $scope.form.name = user.name;
        $scope.form.surname = user.surname;
        $scope.form.phone = user.phone;
        $scope.state = "edit";
    };

    $scope.reset = function() {
        $scope.form.id = '';
        $scope.form.name = '';
        $scope.form.surname = '';
        $scope.form.phone = '';
        $scope.state = "append";
    };

    $scope.add = function(user) {
        console.log(user);
    };

    $scope.remove = function(index, user) {
        Users.delete({ id: user._id }, function () {
            $scope.users.splice(index, 1);
        });
    };
}]);
