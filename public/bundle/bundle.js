(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
angular.module("dbms", [
    "ngResource",
    "ui.router"
]);

angular.module("dbms").config(["$stateProvider", "$urlRouterProvider", "$locationProvider", function($stateProvider, $urlRouterProvider, $locationProvider) {
    $stateProvider.state("home", {
        url: "/",
        templateUrl: "views/home/index.html",
        controller: "HomeController:index" //"HomeIndexController"
    });

    $stateProvider.state("tables", {
        url: "/tables",
        templateUrl: "views/tables/index.html",
        controller: "TablesController"
    });

    $stateProvider.state("books", {
        url: "/books",
        templateUrl: "views/books/index.html",
        controller: "BooksController"
    });

    $stateProvider.state("users", {
        url: "/users",
        abstract: true
    });

    $stateProvider.state("users.index", {
        url: "",
        views: {
            "@": {
                templateUrl: "views/users/index.html",
                controller: "UserController:index"
            }
        }
    });

    $stateProvider.state("users.item", {
        url: "/{id}",
        params: {
            code: {
                squash: false,
                value: null
            }
        },
        views: {
            "@": {
                templateUrl: "views/users/item.html",
                controller: "UserController:item"
            }
        }
    });

    $stateProvider.state("error", {
        url: "/error/{code}",
        params: {
            code: {
                squash: false,
                value: "404"
            }
        },
        templateUrl: "views/error.html",
        controller: ["$scope", "$state", function ($scope, $state) {
            $scope.code = $state.params.code;
        }]
    });

    $urlRouterProvider.otherwise("/error/404");

    $locationProvider.html5Mode(true);
}]);

angular.module("dbms").run(["$rootScope", "$state", function ($rootScope, $state) {
    $rootScope.$on("$stateChangeError", function() {
        $state.go("error", { code: "500" }, { location: false });
    });

    $rootScope.$on("$stateNotFound", function() {
        $state.go("error", { code: "404" }, { location: false });
    });
}]);

},{}],2:[function(require,module,exports){
angular.module("dbms").controller("BooksController", ["$scope", function ($scope) {
    console.log("BooksController");
}]);
},{}],3:[function(require,module,exports){
angular.module("dbms").controller("HomeController:index", ["$scope", function ($scope) {
    console.log("HomeController.index");
}]);

},{}],4:[function(require,module,exports){
angular.module("dbms").controller("TablesController", ["$scope", "$state", "Users", function ($scope, $state, Users) {
    $scope.state = "append";

    Users.query({}, function (users) {
        if(users.length) {
            $scope.users = users;
        } else {
            $state.go("error", { code: "404" }, { location: false });
        }
    });

    $scope.form = {
        id: '',
        name: '',
        surname: '',
        phone: ''
    };

    $scope.save = function(id) {
        console.log('save - ' + id);

        Users.save({ xz: 1 }, {
            qwe: 1,
            cxvbcvb: 1,
            cvbcv: 43
        }, function (response) {
            console.log(response);
        });
    };

    $scope.edit = function(id) {
        console.log('edit - ' + id);
    };

    $scope.delete = function(id) {
        console.log('delete - ' + id);
    };

    $scope.select = function(user) {
        $scope.form.id = user.id;
        $scope.form.name = user.name;
        $scope.form.surname = user.surname;
        $scope.form.phone = user.phone;
        $scope.state = "edit";
    };

    $scope.reset = function() {
        $scope.form.id = '';
        $scope.form.name = '';
        $scope.form.surname = '';
        $scope.form.phone = '';
        $scope.state = "append";
    };

    $scope.add = function(user) {
        console.log(user);
    };

    $scope.remove = function(index, user) {
        Users.delete({ id: user._id }, function () {
            $scope.users.splice(index, 1);
        });
    };
}]);

},{}],5:[function(require,module,exports){
angular.module("dbms").controller("UserController:index", ["$scope", "$state", "Users", function ($scope, $state, Users) {
    Users.query(function (users) {
        if(users.length) {
            $scope.users = users;
        } else {
            $state.go("error", { code: "404" }, { location: false });
        }
    });
}]);

angular.module("dbms").controller("UserController:item", ["$scope", "$state", "Users", function ($scope, $state, Users) {
    Users.get({ id: $state.params.id || 0 }, function (user) {
        if(user._id) {
            $scope.user = user;
        } else {
            $state.go("error", { code: "404" }, { location: false });
        }
    });
}]);

},{}],6:[function(require,module,exports){
require("./boots.js");
require("./services/config.js");
require("./services/users.js");
require("./controllers/home.js");
require("./controllers/user.js");
require("./controllers/tables.js");
require("./controllers/books.js");

},{"./boots.js":1,"./controllers/books.js":2,"./controllers/home.js":3,"./controllers/tables.js":4,"./controllers/user.js":5,"./services/config.js":7,"./services/users.js":8}],7:[function(require,module,exports){
angular.module("dbms").factory("config", ["$location", function ($location) {
    var path = $location.absUrl().replace("" + $location.url(), ''); // #

    if(path.substr(-1) === "/") {
        path = path.substr(0, path.length - 1);
    }

    return {
        api: path + "/api",
        path: path
    }
}]);

},{}],8:[function(require,module,exports){
angular.module("dbms").factory("Users", ["$resource", "config", function($resource, config) {
    return $resource(config.api + "/users/:id");
}]);

},{}]},{},[6])
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJwdWJsaWMvamF2YXNjcmlwdHMvYm9vdHMuanMiLCJwdWJsaWMvamF2YXNjcmlwdHMvY29udHJvbGxlcnMvYm9va3MuanMiLCJwdWJsaWMvamF2YXNjcmlwdHMvY29udHJvbGxlcnMvaG9tZS5qcyIsInB1YmxpYy9qYXZhc2NyaXB0cy9jb250cm9sbGVycy90YWJsZXMuanMiLCJwdWJsaWMvamF2YXNjcmlwdHMvY29udHJvbGxlcnMvdXNlci5qcyIsInB1YmxpYy9qYXZhc2NyaXB0cy9tYWluLmpzIiwicHVibGljL2phdmFzY3JpcHRzL3NlcnZpY2VzL2NvbmZpZy5qcyIsInB1YmxpYy9qYXZhc2NyaXB0cy9zZXJ2aWNlcy91c2Vycy5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtBQ0FBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUNuRkE7QUFDQTtBQUNBOztBQ0ZBO0FBQ0E7QUFDQTtBQUNBOztBQ0hBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDaEVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDbkJBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDUEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FDWkE7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwiYW5ndWxhci5tb2R1bGUoXCJkYm1zXCIsIFtcclxuICAgIFwibmdSZXNvdXJjZVwiLFxyXG4gICAgXCJ1aS5yb3V0ZXJcIlxyXG5dKTtcclxuXHJcbmFuZ3VsYXIubW9kdWxlKFwiZGJtc1wiKS5jb25maWcoW1wiJHN0YXRlUHJvdmlkZXJcIiwgXCIkdXJsUm91dGVyUHJvdmlkZXJcIiwgXCIkbG9jYXRpb25Qcm92aWRlclwiLCBmdW5jdGlvbigkc3RhdGVQcm92aWRlciwgJHVybFJvdXRlclByb3ZpZGVyLCAkbG9jYXRpb25Qcm92aWRlcikge1xyXG4gICAgJHN0YXRlUHJvdmlkZXIuc3RhdGUoXCJob21lXCIsIHtcclxuICAgICAgICB1cmw6IFwiL1wiLFxyXG4gICAgICAgIHRlbXBsYXRlVXJsOiBcInZpZXdzL2hvbWUvaW5kZXguaHRtbFwiLFxyXG4gICAgICAgIGNvbnRyb2xsZXI6IFwiSG9tZUNvbnRyb2xsZXI6aW5kZXhcIiAvL1wiSG9tZUluZGV4Q29udHJvbGxlclwiXHJcbiAgICB9KTtcclxuXHJcbiAgICAkc3RhdGVQcm92aWRlci5zdGF0ZShcInRhYmxlc1wiLCB7XHJcbiAgICAgICAgdXJsOiBcIi90YWJsZXNcIixcclxuICAgICAgICB0ZW1wbGF0ZVVybDogXCJ2aWV3cy90YWJsZXMvaW5kZXguaHRtbFwiLFxyXG4gICAgICAgIGNvbnRyb2xsZXI6IFwiVGFibGVzQ29udHJvbGxlclwiXHJcbiAgICB9KTtcclxuXHJcbiAgICAkc3RhdGVQcm92aWRlci5zdGF0ZShcImJvb2tzXCIsIHtcclxuICAgICAgICB1cmw6IFwiL2Jvb2tzXCIsXHJcbiAgICAgICAgdGVtcGxhdGVVcmw6IFwidmlld3MvYm9va3MvaW5kZXguaHRtbFwiLFxyXG4gICAgICAgIGNvbnRyb2xsZXI6IFwiQm9va3NDb250cm9sbGVyXCJcclxuICAgIH0pO1xyXG5cclxuICAgICRzdGF0ZVByb3ZpZGVyLnN0YXRlKFwidXNlcnNcIiwge1xyXG4gICAgICAgIHVybDogXCIvdXNlcnNcIixcclxuICAgICAgICBhYnN0cmFjdDogdHJ1ZVxyXG4gICAgfSk7XHJcblxyXG4gICAgJHN0YXRlUHJvdmlkZXIuc3RhdGUoXCJ1c2Vycy5pbmRleFwiLCB7XHJcbiAgICAgICAgdXJsOiBcIlwiLFxyXG4gICAgICAgIHZpZXdzOiB7XHJcbiAgICAgICAgICAgIFwiQFwiOiB7XHJcbiAgICAgICAgICAgICAgICB0ZW1wbGF0ZVVybDogXCJ2aWV3cy91c2Vycy9pbmRleC5odG1sXCIsXHJcbiAgICAgICAgICAgICAgICBjb250cm9sbGVyOiBcIlVzZXJDb250cm9sbGVyOmluZGV4XCJcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgICRzdGF0ZVByb3ZpZGVyLnN0YXRlKFwidXNlcnMuaXRlbVwiLCB7XHJcbiAgICAgICAgdXJsOiBcIi97aWR9XCIsXHJcbiAgICAgICAgcGFyYW1zOiB7XHJcbiAgICAgICAgICAgIGNvZGU6IHtcclxuICAgICAgICAgICAgICAgIHNxdWFzaDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICB2YWx1ZTogbnVsbFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICB2aWV3czoge1xyXG4gICAgICAgICAgICBcIkBcIjoge1xyXG4gICAgICAgICAgICAgICAgdGVtcGxhdGVVcmw6IFwidmlld3MvdXNlcnMvaXRlbS5odG1sXCIsXHJcbiAgICAgICAgICAgICAgICBjb250cm9sbGVyOiBcIlVzZXJDb250cm9sbGVyOml0ZW1cIlxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfSk7XHJcblxyXG4gICAgJHN0YXRlUHJvdmlkZXIuc3RhdGUoXCJlcnJvclwiLCB7XHJcbiAgICAgICAgdXJsOiBcIi9lcnJvci97Y29kZX1cIixcclxuICAgICAgICBwYXJhbXM6IHtcclxuICAgICAgICAgICAgY29kZToge1xyXG4gICAgICAgICAgICAgICAgc3F1YXNoOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIHZhbHVlOiBcIjQwNFwiXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIHRlbXBsYXRlVXJsOiBcInZpZXdzL2Vycm9yLmh0bWxcIixcclxuICAgICAgICBjb250cm9sbGVyOiBbXCIkc2NvcGVcIiwgXCIkc3RhdGVcIiwgZnVuY3Rpb24gKCRzY29wZSwgJHN0YXRlKSB7XHJcbiAgICAgICAgICAgICRzY29wZS5jb2RlID0gJHN0YXRlLnBhcmFtcy5jb2RlO1xyXG4gICAgICAgIH1dXHJcbiAgICB9KTtcclxuXHJcbiAgICAkdXJsUm91dGVyUHJvdmlkZXIub3RoZXJ3aXNlKFwiL2Vycm9yLzQwNFwiKTtcclxuXHJcbiAgICAkbG9jYXRpb25Qcm92aWRlci5odG1sNU1vZGUodHJ1ZSk7XHJcbn1dKTtcclxuXHJcbmFuZ3VsYXIubW9kdWxlKFwiZGJtc1wiKS5ydW4oW1wiJHJvb3RTY29wZVwiLCBcIiRzdGF0ZVwiLCBmdW5jdGlvbiAoJHJvb3RTY29wZSwgJHN0YXRlKSB7XHJcbiAgICAkcm9vdFNjb3BlLiRvbihcIiRzdGF0ZUNoYW5nZUVycm9yXCIsIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICRzdGF0ZS5nbyhcImVycm9yXCIsIHsgY29kZTogXCI1MDBcIiB9LCB7IGxvY2F0aW9uOiBmYWxzZSB9KTtcclxuICAgIH0pO1xyXG5cclxuICAgICRyb290U2NvcGUuJG9uKFwiJHN0YXRlTm90Rm91bmRcIiwgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgJHN0YXRlLmdvKFwiZXJyb3JcIiwgeyBjb2RlOiBcIjQwNFwiIH0sIHsgbG9jYXRpb246IGZhbHNlIH0pO1xyXG4gICAgfSk7XHJcbn1dKTtcclxuIiwiYW5ndWxhci5tb2R1bGUoXCJkYm1zXCIpLmNvbnRyb2xsZXIoXCJCb29rc0NvbnRyb2xsZXJcIiwgW1wiJHNjb3BlXCIsIGZ1bmN0aW9uICgkc2NvcGUpIHtcclxuICAgIGNvbnNvbGUubG9nKFwiQm9va3NDb250cm9sbGVyXCIpO1xyXG59XSk7IiwiYW5ndWxhci5tb2R1bGUoXCJkYm1zXCIpLmNvbnRyb2xsZXIoXCJIb21lQ29udHJvbGxlcjppbmRleFwiLCBbXCIkc2NvcGVcIiwgZnVuY3Rpb24gKCRzY29wZSkge1xyXG4gICAgY29uc29sZS5sb2coXCJIb21lQ29udHJvbGxlci5pbmRleFwiKTtcclxufV0pO1xyXG4iLCJhbmd1bGFyLm1vZHVsZShcImRibXNcIikuY29udHJvbGxlcihcIlRhYmxlc0NvbnRyb2xsZXJcIiwgW1wiJHNjb3BlXCIsIFwiJHN0YXRlXCIsIFwiVXNlcnNcIiwgZnVuY3Rpb24gKCRzY29wZSwgJHN0YXRlLCBVc2Vycykge1xyXG4gICAgJHNjb3BlLnN0YXRlID0gXCJhcHBlbmRcIjtcclxuXHJcbiAgICBVc2Vycy5xdWVyeSh7fSwgZnVuY3Rpb24gKHVzZXJzKSB7XHJcbiAgICAgICAgaWYodXNlcnMubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICRzY29wZS51c2VycyA9IHVzZXJzO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICRzdGF0ZS5nbyhcImVycm9yXCIsIHsgY29kZTogXCI0MDRcIiB9LCB7IGxvY2F0aW9uOiBmYWxzZSB9KTtcclxuICAgICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgICAkc2NvcGUuZm9ybSA9IHtcclxuICAgICAgICBpZDogJycsXHJcbiAgICAgICAgbmFtZTogJycsXHJcbiAgICAgICAgc3VybmFtZTogJycsXHJcbiAgICAgICAgcGhvbmU6ICcnXHJcbiAgICB9O1xyXG5cclxuICAgICRzY29wZS5zYXZlID0gZnVuY3Rpb24oaWQpIHtcclxuICAgICAgICBjb25zb2xlLmxvZygnc2F2ZSAtICcgKyBpZCk7XHJcblxyXG4gICAgICAgIFVzZXJzLnNhdmUoeyB4ejogMSB9LCB7XHJcbiAgICAgICAgICAgIHF3ZTogMSxcclxuICAgICAgICAgICAgY3h2YmN2YjogMSxcclxuICAgICAgICAgICAgY3ZiY3Y6IDQzXHJcbiAgICAgICAgfSwgZnVuY3Rpb24gKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKHJlc3BvbnNlKTtcclxuICAgICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgJHNjb3BlLmVkaXQgPSBmdW5jdGlvbihpZCkge1xyXG4gICAgICAgIGNvbnNvbGUubG9nKCdlZGl0IC0gJyArIGlkKTtcclxuICAgIH07XHJcblxyXG4gICAgJHNjb3BlLmRlbGV0ZSA9IGZ1bmN0aW9uKGlkKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2coJ2RlbGV0ZSAtICcgKyBpZCk7XHJcbiAgICB9O1xyXG5cclxuICAgICRzY29wZS5zZWxlY3QgPSBmdW5jdGlvbih1c2VyKSB7XHJcbiAgICAgICAgJHNjb3BlLmZvcm0uaWQgPSB1c2VyLmlkO1xyXG4gICAgICAgICRzY29wZS5mb3JtLm5hbWUgPSB1c2VyLm5hbWU7XHJcbiAgICAgICAgJHNjb3BlLmZvcm0uc3VybmFtZSA9IHVzZXIuc3VybmFtZTtcclxuICAgICAgICAkc2NvcGUuZm9ybS5waG9uZSA9IHVzZXIucGhvbmU7XHJcbiAgICAgICAgJHNjb3BlLnN0YXRlID0gXCJlZGl0XCI7XHJcbiAgICB9O1xyXG5cclxuICAgICRzY29wZS5yZXNldCA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICRzY29wZS5mb3JtLmlkID0gJyc7XHJcbiAgICAgICAgJHNjb3BlLmZvcm0ubmFtZSA9ICcnO1xyXG4gICAgICAgICRzY29wZS5mb3JtLnN1cm5hbWUgPSAnJztcclxuICAgICAgICAkc2NvcGUuZm9ybS5waG9uZSA9ICcnO1xyXG4gICAgICAgICRzY29wZS5zdGF0ZSA9IFwiYXBwZW5kXCI7XHJcbiAgICB9O1xyXG5cclxuICAgICRzY29wZS5hZGQgPSBmdW5jdGlvbih1c2VyKSB7XHJcbiAgICAgICAgY29uc29sZS5sb2codXNlcik7XHJcbiAgICB9O1xyXG5cclxuICAgICRzY29wZS5yZW1vdmUgPSBmdW5jdGlvbihpbmRleCwgdXNlcikge1xyXG4gICAgICAgIFVzZXJzLmRlbGV0ZSh7IGlkOiB1c2VyLl9pZCB9LCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICRzY29wZS51c2Vycy5zcGxpY2UoaW5kZXgsIDEpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfTtcclxufV0pO1xyXG4iLCJhbmd1bGFyLm1vZHVsZShcImRibXNcIikuY29udHJvbGxlcihcIlVzZXJDb250cm9sbGVyOmluZGV4XCIsIFtcIiRzY29wZVwiLCBcIiRzdGF0ZVwiLCBcIlVzZXJzXCIsIGZ1bmN0aW9uICgkc2NvcGUsICRzdGF0ZSwgVXNlcnMpIHtcclxuICAgIFVzZXJzLnF1ZXJ5KGZ1bmN0aW9uICh1c2Vycykge1xyXG4gICAgICAgIGlmKHVzZXJzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAkc2NvcGUudXNlcnMgPSB1c2VycztcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAkc3RhdGUuZ28oXCJlcnJvclwiLCB7IGNvZGU6IFwiNDA0XCIgfSwgeyBsb2NhdGlvbjogZmFsc2UgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSk7XHJcbn1dKTtcclxuXHJcbmFuZ3VsYXIubW9kdWxlKFwiZGJtc1wiKS5jb250cm9sbGVyKFwiVXNlckNvbnRyb2xsZXI6aXRlbVwiLCBbXCIkc2NvcGVcIiwgXCIkc3RhdGVcIiwgXCJVc2Vyc1wiLCBmdW5jdGlvbiAoJHNjb3BlLCAkc3RhdGUsIFVzZXJzKSB7XHJcbiAgICBVc2Vycy5nZXQoeyBpZDogJHN0YXRlLnBhcmFtcy5pZCB8fCAwIH0sIGZ1bmN0aW9uICh1c2VyKSB7XHJcbiAgICAgICAgaWYodXNlci5faWQpIHtcclxuICAgICAgICAgICAgJHNjb3BlLnVzZXIgPSB1c2VyO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICRzdGF0ZS5nbyhcImVycm9yXCIsIHsgY29kZTogXCI0MDRcIiB9LCB7IGxvY2F0aW9uOiBmYWxzZSB9KTtcclxuICAgICAgICB9XHJcbiAgICB9KTtcclxufV0pO1xyXG4iLCJyZXF1aXJlKFwiLi9ib290cy5qc1wiKTtcclxucmVxdWlyZShcIi4vc2VydmljZXMvY29uZmlnLmpzXCIpO1xyXG5yZXF1aXJlKFwiLi9zZXJ2aWNlcy91c2Vycy5qc1wiKTtcclxucmVxdWlyZShcIi4vY29udHJvbGxlcnMvaG9tZS5qc1wiKTtcclxucmVxdWlyZShcIi4vY29udHJvbGxlcnMvdXNlci5qc1wiKTtcclxucmVxdWlyZShcIi4vY29udHJvbGxlcnMvdGFibGVzLmpzXCIpO1xyXG5yZXF1aXJlKFwiLi9jb250cm9sbGVycy9ib29rcy5qc1wiKTtcclxuIiwiYW5ndWxhci5tb2R1bGUoXCJkYm1zXCIpLmZhY3RvcnkoXCJjb25maWdcIiwgW1wiJGxvY2F0aW9uXCIsIGZ1bmN0aW9uICgkbG9jYXRpb24pIHtcclxuICAgIHZhciBwYXRoID0gJGxvY2F0aW9uLmFic1VybCgpLnJlcGxhY2UoXCJcIiArICRsb2NhdGlvbi51cmwoKSwgJycpOyAvLyAjXHJcblxyXG4gICAgaWYocGF0aC5zdWJzdHIoLTEpID09PSBcIi9cIikge1xyXG4gICAgICAgIHBhdGggPSBwYXRoLnN1YnN0cigwLCBwYXRoLmxlbmd0aCAtIDEpO1xyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiB7XHJcbiAgICAgICAgYXBpOiBwYXRoICsgXCIvYXBpXCIsXHJcbiAgICAgICAgcGF0aDogcGF0aFxyXG4gICAgfVxyXG59XSk7XHJcbiIsImFuZ3VsYXIubW9kdWxlKFwiZGJtc1wiKS5mYWN0b3J5KFwiVXNlcnNcIiwgW1wiJHJlc291cmNlXCIsIFwiY29uZmlnXCIsIGZ1bmN0aW9uKCRyZXNvdXJjZSwgY29uZmlnKSB7XHJcbiAgICByZXR1cm4gJHJlc291cmNlKGNvbmZpZy5hcGkgKyBcIi91c2Vycy86aWRcIik7XHJcbn1dKTtcclxuIl19
