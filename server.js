#!/usr/bin/env node

/**
 * Module dependencies.
 */
var http = require('http');
var debug = require('debug')('dbms:server');
var app = require('./app');

/**
 * Create HTTP server.
 */
var server = http.createServer(app);

/**
 * Get port from environment and store in Express.
 */
var port = process.env.PORT || 3000;

/**
 * Listen on provided port, on all network interfaces.
 */
app.set('port', port);

server.listen(port);

server.on('listening', function () {
    var addr = server.address();
    var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;

    console.log('Listening on ' + bind);

    debug('Listening on ' + bind);
});

server.on('error', function (err) {
    if (err.syscall !== 'listen') {
        throw err;
    }

    var bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (err.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw err;
    }
});
