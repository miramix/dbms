var mongoose = require('./../connect');

var schema = new mongoose.Schema({
    title:  String,
    pages: String
});

module.exports = mongoose.model('Books', schema);
