var mongoose = require('./../connect');

var schema = new mongoose.Schema({
    name:  String,
    surname: String,
    phone: String,
    created: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Users', schema);
