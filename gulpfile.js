var gulp = require("gulp");
var utils = require("./gulputils.js");

var projects = {
    main: {
        path: "./public",
        bundle: "{path}/bundle",
        processors: {
            javascript: {
                type: "es5", // ts|es5|es6|coffee
                folders: [
                    "{path}/javascripts/**/*.{type}",
                    "!{path}/javascripts/backups/**/*.{type}"
                ],
                entry: "{path}/javascripts/main.{type}"
            },
            stylesheet: {
                type: "scss", // less|scss
                folders: [
                    "{path}/stylesheets/**/*.{type}",
                    "!{path}/stylesheets/backups/**/*.{type}"
                ],
                entry: "{path}/stylesheets/main.{type}"
            }
        },
        debug: true
    }
};

var main = utils(projects.main);

gulp.task("clean", function () {
    main.clean();
});

gulp.task("build:javascript", function () {
    main.build().javascript();
});

gulp.task("build:stylesheet", function () {
    main.build().stylesheet();
});

gulp.task("build", function () {
    main.build().javascript();
    main.build().stylesheet();
});

gulp.task("watch", ["build"], function () {
    main.watch(["build:javascript"]).javascript();
    main.watch(["build:stylesheet"]).stylesheet();
});

gulp.task("default", function () {
    gulp.start("build");
});
