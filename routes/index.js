var express = require("express");
var router = express.Router();

var users = require("./users");
var tables = require("./tables");
var books = require("./books");

router.get("/", function (req, res) {
    res.json({
        title: "Express"
    });
});

router.use("/users", users);
router.use("/tables", tables);
router.use("/books", books);

module.exports = router;
