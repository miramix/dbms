var express = require("express");
var router = express.Router();

var User = require("./../models/user");

/* GET users listing. */
router.get("/", function (req, res, next) {
    User.find({}, function (err, users) {
        res.json(users);
    });
});

router.get("/:id", function (req, res, next) {
    User.findOne({ _id: req.params.id }, function (err, user) {
        res.json(user);
    });
});

router.post("/", function (req, res, next) {
    res.json({
        body: req.body,
        query: req.query,
        message: "insert"
    })
});

router.post("/:id", function (req, res, next) {
    res.json({
        body: req.body,
        query: req.query,
        message: "update: " + req.params.id
    })
});

router.delete("/:id", function (req, res, next) {
    User.remove({_id: req.params.id}, function(err, affected) {
        if(err) {
            return err;
        }

        res.json({
            affected: affected,
            message: "delete: " + req.params.id
        })
    });
});

module.exports = router;
